
#include <Servo.h> 
#define MOTOR1_CTL1  7  // I1
#define MOTOR1_CTL2  6  // I2
#define MOTOR1_PWM   11 // EnableA
#define MOTOR2_CTL1  5  // I3
#define MOTOR2_CTL2  4  // I4
#define MOTOR2_PWM   10 // EnableB
#define MOTOR_DIR_FORWARD  0
#define MOTOR_DIR_BACKWARD   1
Servo myServo;
int pos = 0;    // variable to store the servo position 
unsigned int rtc = 0; //place holder for keeping track of time for myServo
unsigned int rtp = 0; //storage for the previous time for myServo
unsigned long rtcMotor = 0; //place holder for keeping track of time for Motor
unsigned long rtpMotor = 0; //storage for the previous time for Motor

int straight = 87; //These int are the numbers that the servo will use to look left and right. 
int left = 50; //87 is what my servo needed to look straight, and the other numbers were for left and right
int right = 130; // change them as nessasary to get your servo to work right. The number can be from 0 to 180.
void setup()
{
  // Setup pins for motor 1
  pinMode(MOTOR1_CTL1,OUTPUT);
  pinMode(MOTOR1_CTL2,OUTPUT);
  pinMode(MOTOR1_PWM,OUTPUT);
  // Setup pins for motor 2
  pinMode(MOTOR2_CTL1,OUTPUT);
  pinMode(MOTOR2_CTL2,OUTPUT);
  pinMode(MOTOR2_PWM,OUTPUT);   
  myServo.attach(9);
}


void go(){
  motorStart(MOTOR_DIR_FORWARD);
}

void back(){
  motorStart(MOTOR_DIR_BACKWARD);
}
void setSpeed( char motor_speed)
{
  analogWrite(MOTOR1_PWM, motor_speed);
}

void setLeft()
{
  digitalWrite(MOTOR2_CTL1,HIGH);
  digitalWrite(MOTOR2_CTL2,LOW);
  analogWrite(MOTOR2_PWM, 255);
}

void setRight()
{
  digitalWrite(MOTOR2_CTL1,LOW);
  digitalWrite(MOTOR2_CTL2,HIGH);
  analogWrite(MOTOR2_PWM, 255);
}

void setStraight(){
  analogWrite(MOTOR2_PWM, 0);
  digitalWrite(MOTOR2_CTL1,HIGH);
  digitalWrite(MOTOR2_CTL2,HIGH);
}

void motorStart(byte direction)
{
  switch (direction)
  {
  case MOTOR_DIR_FORWARD:
    {
      digitalWrite(MOTOR1_CTL1,HIGH);
      digitalWrite(MOTOR1_CTL2,LOW);          
    }
    break; 

  case MOTOR_DIR_BACKWARD:
    {
      digitalWrite(MOTOR1_CTL1,LOW);
      digitalWrite(MOTOR1_CTL2,HIGH);          
    }
    break;         
  }
}

void Stop()
{
  setSpeed(0);
  digitalWrite(MOTOR1_CTL1,HIGH);
  digitalWrite(MOTOR1_CTL2,HIGH);     
}






void loop()
{
  // Start motors!
  rtc = millis()-rtp; 
  rtcMotor = millis()-rtpMotor;

  switch (rtcMotor) {
  case 0 ... 1000:
    go();
    setSpeed(100);
    break;

  case 1001 ... 2000:
    setRight();
    setSpeed(255);
    break;

  case 2001 ... 3000:
    back();
    break; 
  case 3001 ... 4001:
    setLeft();
    break;
    
  default:
    rtpMotor = rtpMotor + rtcMotor;//gets the timer to reset when it gets past 3 sg.
    break;
  }

  // sweep the servo holding the sensor:
  switch (rtc){ 
  case 0 ... 200:
    myServo.write(left); 
    break;

  case 201 ... 400:
    myServo.write(straight);
    break;

  case 401 ... 600:
    myServo.write(right);
    break;

  case 601 ... 800:
    myServo.write(straight);
    break;

  default:
    rtp = rtp+rtc; //gets the timer to reset when it gets past 800
    break;
  }






}

